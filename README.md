ispconfig2_move_junk
====================

Modification to ISPConfig 2 to move Junk mail to folder (.JunkMail)

This patch also contains code to auto create standard Maildir folders:

``
.Drafts
.Trash
.JunkMail
.Sent
``

Auto IMAP subscription to the above folders.

Tested on ISPConfig 2 (2.2.40) Will most likely work on all 2.2.23 versions or later


Setup
=================
Before copying the files contained over the original backup your server and do not perform on a Production system before it has been tested in your environment.

1)
The first step is to log into your ISPConfig Panel with the Admin account. Then enter the Administration tab and open the Form Designer with Edit Form. Search for isp - ISP User and click edit.
You should now see the complete form description of that particular form. Look out for spam_strategy and hit edit again.
Now change the Value list of that form element by inserting a line for our future move option so it shows:
```
accept,accept;
move,move;
discard,discard;
```

and save that form. Your mail user options in your domain should now show that new option in the spam strategy dropdown box - even though it has no function yet. Now for the console part: Let's edit some ISPConfig files, to breathe life into the new option.


2) Apply the source files included. Overwrite existing files and copy templates into the approiate places.


More documentation to follow.



